# Blog built through Django

## Abstract 
A simple blog developed through Django 3 / Python. This project is a fork of [Antonio Mele's code](https://github.com/PacktPublishing/Django-3-by-Example)


## Premise
This project requires :

* Python 3.8
* PosgreSql 12.x

They have to be up and running. 

## Installation
* Python Virtual Environment (venv) from a terminal (bash shell):

>> **mkdir /var/www/html/myblog**

>> **chmod -R 777 /var/www/html/myblog**

>> **cd /var/www/html/myblog**

>> **python -m venv venv**

>> **source venv/bin/activate**

>> **pip install "Django==3.1.5"**

>> *  In the venv, download and install on your local computer:
>>> * asgiref              3.3.1
>>> * django-debug-toolbar 3.2
>>> * django-taggit        1.2.0
>>> * psycopg2-binary      2.8.6
>>> * pytz                 2020.4
>>> * setuptools           41.6.0
>>> * sqlparse             0.4.1

>> **django-admin startproject mysite**

>> **cd mysite**

>> **python manage.py migrate**

>> **python manage.py createsuperuser** 

>>> and follow the procedure on the screen

* The root folder of the project is 
 
    >**myblog** 
    
* and the absolute path is
 
    >**/var/www/html/myblog**
    
* Clone the project from gitlab on your Desktop :
    
    >**git@gitlab.com:greengo_axe/myblog.git**

* copy and overwrite all folders and files from 

>>**Desktop/myblog**

>into 

>>**/var/www/html/myblog**

* Delete the folder on your Desktop (**Desktop/myblog**)

* Start the Django's built-in server: 

> **/var/www/html/myblog/mysite>$ python manage.py runserver**

* Open your favourite browser and type this URL:

> **localhost:8000**

* Enjoy!


## Troubleshooting
  * This project has been developed on Linux OS and there might be a few problems with the path or to grant the right permissions on Windows / Apple OSs.
  
 
## TODO List:
 * To fix CSS bugs
 
 * To improve validation on every form.
 
 * Whatever you like
